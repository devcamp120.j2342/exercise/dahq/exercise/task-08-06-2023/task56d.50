package com.devcamp.bookauthorapi.model;

import java.util.ArrayList;

public class Book {
    private String name;
    private int qty = 0;
    private double price;
    private ArrayList<Author> authors;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public ArrayList<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(ArrayList<Author> authors) {
        this.authors = authors;
    }

    public Book(String name, int qty, double price) {
        this.name = name;
        this.qty = qty;
        this.price = price;
    }

    public Book() {
    }

    public Book(String name, int qty, double price, ArrayList<Author> authors) {
        this.name = name;
        this.qty = qty;
        this.price = price;
        this.authors = authors;
    }

    @Override
    public String toString() {
        return "Book [name=" + name + ", qty=" + qty + ", price=" + price + ", authors=" + authors + "]";
    }

    public String getAuthorName() {
        String name = "";
        for (int i = 0; i < authors.size(); i++) {
            name += authors.get(i).getName() + ",";

        }
        return name;

    }

}
