package com.devcamp.bookauthorapi.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.bookauthorapi.model.Book;

@Service
public class BookService {
    Book bookA = new Book("Book A", 10, 190.6);
    Book bookB = new Book("Book B", 18, 50.6);
    Book bookC = new Book("Book C", 15, 90.6);
    Book bookD = new Book("Book D", 15, 90.6);
    Book bookE = new Book("Book E", 19, 80.6);
    Book bookF = new Book("Book F", 14, 37.6);
    @Autowired
    private AuthorService authorService;

    public ArrayList<Book> allBooks() {
        bookA.setAuthors(authorService.authorsA());
        bookB.setAuthors(authorService.authorsB());
        bookC.setAuthors(authorService.authorsC());
        bookD.setAuthors(authorService.authorsC());
        bookE.setAuthors(authorService.authorsB());
        bookF.setAuthors(authorService.authorsA());

        ArrayList<Book> books = new ArrayList<>();
        books.add(bookA);
        books.add(bookB);
        books.add(bookC);
        books.add(bookD);
        books.add(bookE);
        books.add(bookF);

        return books;

    }

    public ArrayList<Book> findBooksByQuatity(double paramqty) {
        ArrayList<Book> books = allBooks();
        ArrayList<Book> arrayResult = new ArrayList<>();

        for (Book book : books) {
            if (book.getQty() >= paramqty) {
                arrayResult.add(book);
            }
        }
        return arrayResult;

    }

    public Book bookIndex(int paramIndex) {
        ArrayList<Book> books = allBooks();
        Book book = new Book();
        if (paramIndex < -1 || paramIndex > 5) {
            return book;
        } else {
            book = books.get(paramIndex);
        }
        return book;
    }

}
