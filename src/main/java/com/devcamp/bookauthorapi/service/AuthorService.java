package com.devcamp.bookauthorapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.bookauthorapi.model.Author;

@Service
public class AuthorService {
    Author authorA1 = new Author("Nguyen Van A", "a@gmail.com", 'm');
    Author authorA2 = new Author("Nguyen Van B", "b@gmail.com", 'm');
    Author authorA3 = new Author("Nguyen Van C", "c@gmail.com", 'f');
    Author authorA4 = new Author("Nguyen Van D", "d@gmail.com", 'm');
    Author authorA5 = new Author("Nguyen Van E", "e@gmail.com", 'f');
    Author authorA6 = new Author("Nguyen Van F", "f@gmail.com", 'f');

    public ArrayList<Author> authorsA() {
        ArrayList<Author> authors = new ArrayList<>();
        authors.add(authorA1);
        authors.add(authorA2);
        return authors;

    }

    public ArrayList<Author> authorsB() {
        ArrayList<Author> authors = new ArrayList<>();
        authors.add(authorA3);
        authors.add(authorA4);
        return authors;

    }

    public ArrayList<Author> authorsC() {
        ArrayList<Author> authors = new ArrayList<>();
        authors.add(authorA5);
        authors.add(authorA6);
        return authors;

    }

    public Author findAuthorByEmail(String paramEmail) {
        ArrayList<Author> authors = new ArrayList<>();
        Author authorRe = new Author();
        authors.addAll(authorsA());
        authors.addAll(authorsB());
        authors.addAll(authorsC());
        for (Author author : authors) {
            if (author.getEmail().equals(paramEmail)) {
                authorRe = author;
            }

        }
        return authorRe;

    }

    public ArrayList<Author> findAuthorByGender(char gender) {
        ArrayList<Author> authors = new ArrayList<>();
        ArrayList<Author> authorsResult = new ArrayList<>();

        authors.addAll(authorsA());
        authors.addAll(authorsB());
        authors.addAll(authorsC());
        for (Author author : authors) {
            if (author.getGender() == gender) {
                authorsResult.add(author);
            }

        }
        return authorsResult;

    }

}
