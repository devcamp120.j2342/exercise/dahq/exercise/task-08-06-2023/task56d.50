package com.devcamp.bookauthorapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.bookauthorapi.model.Book;
import com.devcamp.bookauthorapi.service.BookService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class BookController {
    @Autowired
    private BookService bookService;

    @GetMapping("/books")
    public ArrayList<Book> findAllBooks() {
        ArrayList<Book> books = bookService.allBooks();
        return books;
    }

    @GetMapping("/book-quantity")
    public ArrayList<Book> finBooksByQuaty(@RequestParam(required = true, name = "qty") double quantity) {
        ArrayList<Book> books = bookService.findBooksByQuatity(quantity);
        return books;
    }

    @GetMapping("/books/{bookId}")
    public Book bookId(@PathVariable int bookId) {
        Book book = bookService.bookIndex(bookId);
        return book;
    }

}
