package com.devcamp.bookauthorapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.bookauthorapi.model.Author;
import com.devcamp.bookauthorapi.service.AuthorService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class AuthorController {
    @Autowired
    private AuthorService authorService;

    @GetMapping("/author-info")
    public Author finAuthor(@RequestParam(required = true, name = "email") String email) {
        Author author = authorService.findAuthorByEmail(email);
        return author;
    }

    @GetMapping("/author-gender")
    public ArrayList<Author> finAuthorsGender(@RequestParam(required = true, name = "gender") char gender) {
        ArrayList<Author> authors = authorService.findAuthorByGender(gender);
        return authors;
    }

}
